function NoteModel() {
  const notes = [];

  /**
   * addNote
   * @param {note: string} payload
   */
  function addNote(payload) {
    notes.push(payload);
  }

  /**
   * removeNote
   * @param index: number
   */
  function removeNote(index) {
    notes.splice(index, 1);
  }

  function removeNotes() {
    notes.splice(0, notes.length);
  }

  return {
    notes,
    addNote,
    removeNote,
    removeNotes,
  };
}

function CabeApp() {
  const notesModel = NoteModel();
  const formAddNote = document.querySelector('section#section-note-form > .wrapper > form');
  const noteInput = document.querySelector('input[name=note]');
  const sectionNoteItems = document.getElementById('section-note-items');
  const sectionNoData = document.querySelector('section#section-no-data');
  const sectionBtnClear = document.querySelector('section#section-btn-clear');
  const buttonRemoveNotes = document.getElementById('clear-button');

  sectionBtnClear.style.display = 'none';

  function formValidation() {
    if (noteInput.value === '') {
      alert('Note cannot be empty');
      return false;
    }

    return true;
  }

  function handleSubmit(e) {
    e.preventDefault();

    const formIsValid = formValidation();
    if (formIsValid) {
      const payload = {
        note: noteInput.value,
      };

      notesModel.addNote(payload);
      renderTaskItem();
    }
  }

  function renderTaskItem() {
    const renderItems = [];

    if (notesModel.notes.length > 0) {
      sectionNoteItems.style.display = 'block';
      sectionNoData.style.display = 'none';
      sectionBtnClear.style.display = 'block';
    } else {
      sectionNoData.style.display = 'block';
      sectionBtnClear.style.display = 'none';
    }

    for (let i = 0; i < notesModel.notes.length; i++) {
      renderItems.push(`
            <div class="todo-item my-3">
                <div>
                    <strong>${i + 1} - ${notesModel.notes[i].note} </strong>
                </div>
                <div>
                    <a href="#" class="remove-item" data-index="${i}" >remove</a>
                </div>
            </div>
        `);
    }

    sectionNoteItems.innerHTML = renderItems.join(' ');

    document.querySelectorAll('.remove-item').forEach(function (removeItem) {
      removeItem.onclick = function (event) {
        const indexTask = parseInt(event.target.getAttribute('data-index'));
        notesModel.removeNote(indexTask);
        renderTaskItem();
      };
    });
  }

  function handleClearAllNotes() {
    notesModel.removeNotes();
    if (notesModel.notes.length === 0) {
      sectionNoteItems.style.display = 'none';
      sectionNoData.style.display = 'block';
      sectionBtnClear.style.display = 'none';
    }
  }

  formAddNote.onsubmit = handleSubmit;
  buttonRemoveNotes.onclick = handleClearAllNotes;
}

CabeApp();
